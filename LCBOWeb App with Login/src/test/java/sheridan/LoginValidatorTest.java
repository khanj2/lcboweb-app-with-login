package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegularWithAlphaNumeric( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ram5e5" ) );
	}
	
	@Test
	public void testIsValidLoginRegularWithLength( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}

	@Test
	public void testIsValidLoginExceptionWithAlphaNumeric( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Ram$es" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionWithLength( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryInWithAlphaNumeric( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "a43535" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryInWithLength( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutWithAlphaNumeric( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "7Am53s" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutWithLength( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "rams." ) );
	}

}
