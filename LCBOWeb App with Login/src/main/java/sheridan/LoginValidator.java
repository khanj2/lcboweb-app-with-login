package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return loginName != null && loginName.matches("^[a-zA-Z][a-zA-Z0-9]*$")  
				&& loginName.trim().length( ) >= 6 ;
	}
}
